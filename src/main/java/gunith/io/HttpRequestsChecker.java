package gunith.io;

import gunith.io.domain.HttpRequestsInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.jayway.jsonpath.JsonPath;

public class HttpRequestsChecker {

	private static final String LINE_SEP = System.getProperty("line.separator");

	public void checkFile(String srcFileName, String destFileName) throws IOException {
		String[] lines = readFileAndBreakContentToLines(new File(srcFileName));

		List<HttpRequestsInfo> infos = check(Arrays.asList(lines));

		PrintWriter writer = new PrintWriter(destFileName);
		for (HttpRequestsInfo info : infos) {
			for (String called : info.getCalled()) {
				writer.append(info.getIntended() + "," + called);
				writer.append(LINE_SEP);
			}
		}
		writer.flush();
		writer.close();
	}

	public List<HttpRequestsInfo> check(List<String> urls) throws IOException {

		File logFolder = new File("logs" + File.separator + new Date().getTime());
		WebDriver driver = initFirefox(logFolder);
		for (String url : urls) {

			try {
				driver.get(url);
				Thread.sleep(10000);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			}
		}
		driver.quit();

		Map<String, List<String>> calledUrls = parseJsonLogs(logFolder);

		List<HttpRequestsInfo> infos = new ArrayList<HttpRequestsInfo>();
		for (Entry<String, List<String>> entry : calledUrls.entrySet()) {
			infos.add(new HttpRequestsInfo(entry.getKey(), entry.getValue()));
		}
		return infos;
	}

	private WebDriver initFirefox(File logFolder) {
		String logFolderPath = logFolder.getAbsolutePath();

		String pathname = "src" + File.separator + "main" + File.separator + "resources" + File.separator;
		File firebug = new File(pathname + "firebug-1.12.8.xpi");
		File netExport = new File(pathname + "netExport-0.9b6.xpi");

		FirefoxProfile profile = new FirefoxProfile();
		try {
			profile.addExtension(firebug);
			profile.addExtension(netExport);
		} catch (IOException e) {
			e.printStackTrace();
		}

		profile.setPreference("app.update.enabled", false);

		// Setting Firebug preferences
		profile.setPreference("extensions.firebug.currentVersion", "2.0");
		profile.setPreference("extensions.firebug.addonBarOpened", true);
		profile.setPreference("extensions.firebug.console.enableSites", true);
		profile.setPreference("extensions.firebug.script.enableSites", true);
		profile.setPreference("extensions.firebug.net.enableSites", true);
		profile.setPreference("extensions.firebug.previousPlacement", 1);
		profile.setPreference("extensions.firebug.allPagesActivation", "on");
		profile.setPreference("extensions.firebug.onByDefault", true);
		profile.setPreference("extensions.firebug.defaultPanelName", "net");

		// Setting netExport preferences
		profile.setPreference("extensions.firebug.netexport.alwaysEnableAutoExport", true);
		profile.setPreference("extensions.firebug.netexport.autoExportToFile", true);
		profile.setPreference("extensions.firebug.netexport.Automation", true);
		profile.setPreference("extensions.firebug.netexport.showPreview", false);


		logFolder.mkdirs();
		profile.setPreference("extensions.firebug.netexport.defaultLogDir", logFolderPath);

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName("firefox");
		capabilities.setPlatform(org.openqa.selenium.Platform.ANY);
		capabilities.setCapability(FirefoxDriver.PROFILE, profile);

		WebDriver driver = new FirefoxDriver(capabilities);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		return driver;
	}

	public Map<String, List<String>> parseJsonLogs(File logFolder) throws IOException {
		File[] files = logFolder.listFiles();
		Map<String, List<String>> fullUrlsList = new HashMap<String, List<String>>();
		for (File file : files) {
			// Scanner scanner = new Scanner(file);
			// while (scanner.hasNext()) {
			// String line = scanner.next();
			// //if (!line.trim().startsWith("\"text\": ")){
			//
			// json.append(line);
			// //}
			// }
			// scanner.close();
			// int paranthesisCnt = 0;
			// FileReader reader = new FileReader(file);
			// do {
			// char character = (char) reader.read();
			// json.append(character);
			// if (character == '{')
			// paranthesisCnt++;
			// else if (character == '}')
			// paranthesisCnt--;
			// } while (paranthesisCnt > 0);
			// reader.close();

			String[] lines = readFileAndBreakContentToLines(file);
			StringBuilder noTextJson = new StringBuilder();
			for (String line : lines) {
				if (!line.trim().startsWith("\"text\": ")) {
					noTextJson.append(line);
					noTextJson.append(LINE_SEP);
				}
			}

			List<String> urls;
			// File testOutputFile = new File(file.getAbsolutePath() +
			// "cleaned.json");
			// try {
			// FileWriter writer = new FileWriter(testOutputFile);
			// writer.write(noTextJson.toString());
			// writer.flush();
			// writer.close();
			// } catch (IOException e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }

			urls = extractUrls(noTextJson);
			fullUrlsList.put(urls.get(0), urls);
		}
		return fullUrlsList;
	}

	private String[] readFileAndBreakContentToLines(File file) throws FileNotFoundException, IOException {
		StringBuilder origJson = new StringBuilder();
		FileReader reader = new FileReader(file);
		int character = (char) 0;
		do {
			character = reader.read();
			origJson.append((char) character);
		} while (character != -1);
		reader.close();

		String[] lines = origJson.toString().split("\n");
		return lines;
	}

	private List<String> extractUrls(StringBuilder json) {
		List<String> urls;
		urls = JsonPath.read(json.toString(), "$.log.entries[*].request.url");
		return urls;
	}

}
