package gunith.io.domain;

import java.util.List;

public class HttpRequestsInfo {
	private String intended;
	private List<String> called;

	public HttpRequestsInfo(String intended, List<String> called) {
		super();
		this.intended = intended;
		this.called = called;
	}

	/**
	 * @return the intended
	 */
	public String getIntended() {
		return intended;
	}

	/**
	 * @return the called
	 */
	public List<String> getCalled() {
		return called;
	}

}
