package gunith.io.runner;

import gunith.io.HttpRequestsChecker;

import java.io.IOException;
import java.util.Arrays;

public class HttpRequestsCheckerRunnerForFrogman {
	public static void main(String[] args) throws IOException {
		new HttpRequestsChecker().check(Arrays.asList("http://www.thefrogman.me")); // http://www.gstatic.com/codesite/ph/images/defaultlogo.png
	}
}
