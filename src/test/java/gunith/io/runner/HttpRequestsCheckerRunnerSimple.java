package gunith.io.runner;

import gunith.io.HttpRequestsChecker;

import java.io.IOException;
import java.util.Arrays;

public class HttpRequestsCheckerRunnerSimple {
	public static void main(String[] args) throws IOException {
		new HttpRequestsChecker().check(Arrays.asList("http://www.gstatic.com/codesite/ph/images/defaultlogo.png")); // http://www.gstatic.com/codesite/ph/images/defaultlogo.png
	}
}
